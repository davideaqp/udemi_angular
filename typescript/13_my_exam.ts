(()=>{

    // Uso de Let y Const
    var nombre = 'Ricardo ';
    var apellido = 'Tapia';
    var apellidos2 = 'Tapia3';
    var edad = 23;
  
    var PERSONAJE = {
      nombre: nombre,
      edad: edad
    };
      //mi solucion
    const nombre1 = 'david h';
    const edad1 = 26;

    let personaje1 ={
        nombre:String,
        edad:Number
    }
    //solucionario
    const nombre2 = 'david h';
    const edad2 = 26;

    let personaje2 ={nombre,edad}




    // Cree una interfaz que sirva para validar el siguiente objeto
    var batman = {
      nombre: 'Bruno Díaz',
      artesMarciales: ['Karate','Aikido','Wing Chun','Jiu-Jitsu']
    }
    //mi solucion
    /*const batman1 ={
        nombre1:'bruno diaz',
        artesmarciales: string[] = ['karate', 'aikido','wingchun','jiu-jitsu']


    }*/
    //solucionario 
    interface Batman{
        nombre:string,
        artesMarciales:string[]
    }
    const batman2:Batman ={
        nombre:'bruno diaz',
        artesMarciales: ['karate', 'aikido','wingchun','jiu-jitsu']


    }



    // Convertir esta funcion a una funcion de flecha
    function resultadoDoble( a:number, b:number ){
      return (a + b) * 2
    }
    //mi solucion
    const resultadoDoble1 = (a:number,b:number)=>(a+b)*2;
  
    //solucion
    const resultadoDoble2 = ( a:number, b:number ):number=>{
        return (a + b) * 2
      }




    // Función con parametros obligatorios, opcionales y por defecto
    // donde NOMBRE = obligatorio
    //       PODER  = opcional
    //       ARMA   = por defecto = 'arco'
    function getAvenger( nombre:string, poder:string, arma:string ){
      var mensaje;
      if( poder ){
        mensaje = nombre + ' tiene el poder de: ' + poder + ' y un arma: ' + arma;
      }else{
        mensaje = nombre + ' tiene un ' + poder
      }
    };
    //mi solucion
    /*function getAvenger1(
        nombre:string,
        poder?:string,
        arma:'arco'
    ){
        if(nombre){
            console.log(`${nombre} tiene el poder de ${poder} y un arma ${arma}`)
        }else{
            console.log(`${nombre} tiene un arma ${arma}`);
        }

    }*/
    //solucion
    function getAvenger2(
        nombre:string,
        poder?:string,
        arma:string='arco'
    ){
        let mensaje
        if(poder){
            mensaje =`${nombre} tiene el poder de ${poder} y un arma ${arma}`
        }else{
            mensaje = `${nombre} tiene un arma ${arma}`
        }

    }

   



    // Cree una clase que permita manejar la siguiente estructura
    // La clase se debe de llamar rectangulo,
    // debe de tener dos propiedades:
    //   * base
    //   * altura
    // También un método que calcule el área  =  base * altura,
    // ese método debe de retornar un numero.
    //mi solucion 
    class Rectangulo{
        constructor(
            public base:number,
            public altura:number
        ){ }
        calacularArea(){
            return this.base*this.altura;
        }
    }
    const miRectangulo =new Rectangulo(2,3);
    miRectangulo.calacularArea();
    //solucion
    class Rectangulo1{
        constructor(
            public base:number,
            public altura:number
        ){}
        calcularArea = ():number =>this.base * this.altura;
    }
  })();