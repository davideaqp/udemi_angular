(()=>{

interface Xmen{
    nombre: string;
    edad: number;
    poder?: string;
}

const enviarMision =(xmen: Xmen)=>{
    console.log(`Enviando a ${xmen.nombre} a la misión de Grand Chase`);
}
const regresarCuartel =(xmen: Xmen)=>{
    console.log(`Trayendo de vuelta a ${xmen.nombre} de la misión de Grand Chase`);
}

const deadpool: Xmen = {
    nombre: "Dead Pool",
    edad: 60
}
const coloso: Xmen = {
    nombre: "Coloso",
    edad: 26
}

enviarMision(deadpool);
regresarCuartel(coloso);

})();

