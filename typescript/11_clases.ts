(()=>{

class Avenger {

    // nombre: String;
    // equipo: String;
    // nombreReal: String;

    // puedePelear: Boolean;
    // peleasGanadas: Number;

    //constructor(nombre: string){
    constructor(public nombre: string,
        public equipo: string,
        public nombreReal?: string,
        public puedePelear: boolean = true,
        public peleasGanadas: number = 2){

    //     nombre: String;
    //     this.nombre = nombre;
    //     this.equipo = equipo;
    //     this.nombreReal = nombreReal;
    //     this.puedePelear = puedePelear;
    //     this.peleasGanadas = peleasGanadas;
    }
}

const antman = new Avenger('antman','capi');

console.log(antman);

})();

