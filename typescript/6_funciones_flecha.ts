(function(){

    const miFuncion = function(a: string){
        return a.toUpperCase();   
   }
   // solo para funciones de un solo argumento
   const miFuncionF = (a: string) =>a.toUpperCase();
   
   console.log(miFuncion('normal'));
   console.log(miFuncionF('flecha'));
   
    // funciones normales y de flecha para sumar

   const sumarN = function (a: number, b: number){
       return a+b;
   }
   const sumarNums = (a:number,b:number) => a+b;

   console.log(sumarN(2,3));
   console.log(sumarNums(2,6));

    const hulk = {
        nombre: 'Hulk',
        smash(){
            //se demora en ejecutar 5 segundos
            setTimeout( ()=>{
                console.log(`${this.nombre} smash!!!`);
            }, 1000 );
        }
    }
    hulk.smash();
})();



