(function(){

    let mensaje: string = "Hola";
    let numero: number = 123;
    let booleano: boolean = true;
    let hoy: Date = new Date();
    
    let cualquiercosa;
    cualquiercosa = mensaje;
    cualquiercosa = numero;
    cualquiercosa = booleano;
    cualquiercosa = hoy;

    //Variante simplificada
    let post = "hello";
    let num = 12345
    let bool = false;
    let fecha = new Date();

    let anyThing: string | number | Date |boolean;
    anyThing = post;
    anyThing = num;
    anyThing = bool;
    anyThing = fecha;

})();